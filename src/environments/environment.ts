// This file can be replaced during build by using the `fileReplacements` array.
// `ng build ---prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase:{
    apiKey: "AIzaSyCR_Zkw2XXcMsmSYhSfeN7S5wdVWN2I7KI",
    authDomain: "shoppingcar-c2557.firebaseapp.com",
    databaseURL: "https://shoppingcar-c2557.firebaseio.com",
    projectId: "shoppingcar-c2557",
    storageBucket: "shoppingcar-c2557.appspot.com",
    messagingSenderId: "670145566395"
  }
};

/*
 * In development mode, to ignore zone related error stack frames such as
 * `zone.run`, `zoneDelegate.invokeTask` for easier debugging, you can
 * import the following file, but please comment it out in production mode
 * because it will have performance impact when throw error
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
